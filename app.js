
// Import all required modules
const 
    express          = require('express'),
    bodyParser       = require('body-parser');
    mongoose         = require('mongoose');
    routesHandler    = require('./routesHandler');
    utils            = require('./helpers/utils');
    mongoSanitize    = require('express-mongo-sanitize');


class App {

    // Initialize the appInstance member of App class with express module to be served in the project.
    constructor() {
        this.appInstance = express();
        this.loadMiddlewares();
        this.loadDB();
    }

    loadMiddlewares() {

        //Load body parser middleware here
        this.appInstance.use(bodyParser.urlencoded({extended:false}));
        this.appInstance.use(bodyParser.json());
        this.appInstance.use(bodyParser.raw());
        this.appInstance.use(bodyParser.text());

        //Add CORS to appInstance
        this.appInstance.use((req, res, next) => {

            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "*");
            
            if (req.method === 'OPTIONS') {
                res.header('Access-Control-Allow-Methods', '*');
                return res.status(utils.getReqStatus().success).json({});
            }
            next();
        });

        // Load MongoSanitizer
        this.appInstance.use(mongoSanitize());

        // Initialize routesHandler
        routesHandler.init(this.appInstance);

        //Error Handling File Not found
        this.appInstance.use((req, res, next) => {
            
            res.status(utils.getReqStatus().not_found);
            res.json(utils.getResponseJson(true,{},"404 File Not Found"))

        });

        //Error Handling for all functions
        this.appInstance.use((err, req, res, next) => {

            res.status(err.status || utils.getReqStatus().error);
            res.json(utils.getResponseJson(true,{},err.message));
        });

    }

    // Mongo Config and setup
    loadDB(){
        const dbPath    = process.env.dbPath || 'mongodb://localhost/pokemon';
        const options   = {useNewUrlParser: true, useUnifiedTopology: true}
        const mongo     = mongoose.connect(dbPath, options);

        mongo.then(()=>{
            console.log('connected');
        },error => {
            console.log('error mongo connection');
        }) 
    }
}

// Export only appInstance member with express instantiated to the server.js file
module.exports = new App().appInstance;