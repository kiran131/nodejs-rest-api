const routerObject = require('express').Router();
let pokemonController = require('./controller/pokemonController')

class RoutesHandler {

    // Initialize appInstance with appInstance param from app.js file
    // Also create a router memeber with express router object of RoutesHandler class
    init (appInstance){
        this.appInstance = appInstance;
        this.router      = routerObject;

        this.configRoutesV1();
        this.bindRoutes();
    }

    // Register the routes here to respective http method and controller functions
    configRoutesV1(){
        this.router.route('/pokemonApi/v1/pokemons')
                   .get(pokemonController.get)
                   .post(pokemonController.add);
        
        this.router.route('/pokemonApi/v1/pokemons/:id')
                   .get(pokemonController.getByID)
                   .put(pokemonController.updateByID)
                   .delete(pokemonController.removeByID);
    }

    // Binding all the routes with the appInstance
    bindRoutes(){
        this.appInstance.use(this.router);
    }
}

module.exports = new RoutesHandler();
