// Load AppInstance from App.js
const App = require('./app');
let port  = process.env.PORT || 3000;

// Create Server
App.listen(port, () => { console.log("Started server at http://localhost:" + port); })