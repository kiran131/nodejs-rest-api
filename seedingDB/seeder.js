const mongoose = require('mongoose');
const fs = require('fs');

const dbPath = 'mongodb://localhost/pokemon';
const options = {useNewUrlParser: true, useUnifiedTopology: true}
const mongo = mongoose.connect(dbPath, options);

const data = fs.readFileSync('./seedingDB/seedData.json');
const entries = JSON.parse(data.toString());

mongo.then(()=>{
    console.log('connected');
},error => {
    console.log('error mongo connection');
}) 

const Pokemon = require('../models/pokemonModel');
const { exit } = require('process');

Pokemon.find((err,items)=>{
    if(items.length>0) {
        entries.forEach(function (entry) {
            
            if(items.some(item => item.Name !== entry.Name)) {
                Pokemon.create(entry, (err,result)=> {
                    if (err) throw err;
                    console.log("Inserted: ", result.insertedCount);
                })
            }
        })
    }
    else {
        if(entries.length>0) {
            Pokemon.collection.insertMany(entries,(err,result)=>{
                if (err) throw err;
                console.log('Inserted docs:', result.insertedCount);
            });
        } else {
            Pokemon.create(entries, (err,result)=> {
                if (err) throw err;
                console.log("Inserted: ", result.insertedCount);
            })
        }
    }
})

exit();


