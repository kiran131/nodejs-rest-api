const mongoose = require('mongoose');

let pokemonSchema = mongoose.Schema({
    Name: {type: String, required: true},
    MaxCP: {type: Number, required: true},
    Attack: {type: Number},
    Defence: {type: Number},
    Stamina: {type: Number},
    Description: {type: String},
    Family: {type: String},
    Weaknesses: {type: String},
    Resistances: {type: String}
});

const Pokemon = mongoose.model('Pokemon', pokemonSchema);

module.exports = Pokemon;
