/* 
* Controller Class for CRUD Operations using mongoose orm module
* Using "utils" to create custom helper methods to sanitize and get default response json 
*/

const utils   = require('../helpers/utils');
const Pokemon = require('../models/pokemonModel');


class pokemonController {

    default (req,res,next) {
        res.send("You're Home");
    }

    // Get all records
    get (req,res,next) {
        Pokemon.find((err,items)=>{
            if (err){
                res.status(utils.getReqStatus().error).json(utils.getResponseJson(true,{},err.message))
            }
            res.status(utils.getReqStatus().success).json(utils.getResponseJson(false,items,"Retrived!"))
        })
    }

    // Create a record
    add (req,res,next) {
        let reqPokemon = utils.noInfections(req);

        if (!reqPokemon.Name || 
            (!reqPokemon.MaxCP) && isInteger(reqPokemon.MaxCP) ||
            (!reqPokemon.Attack) && isInteger(reqPokemon.Attack) ||
            (!reqPokemon.Defence) && isInteger(reqPokemon.Defence) ||
            (!reqPokemon.Stamina) && isInteger(reqPokemon.Stamina)) {
            res.status(utils.getReqStatus().invalid).json(utils.getResponseJson(true,{},"Validation Error"))
        }

        Pokemon.create(reqPokemon,(err,result) => {
            if (err){
                res.status(utils.getReqStatus().error).json(utils.getResponseJson(true,{},err.message))
            }
            res.status(utils.getReqStatus().success).json(utils.getResponseJson(false,result,"Created!"))
        })
    }

    // Get a record by ID
    getByID (req,res,next) {
        let pokemonID = req.params.id;
        Pokemon.findById(pokemonID,(err,pokemon)=>{
            if(err || !pokemon){
                res.status(err?utils.getReqStatus().error:utils.getReqStatus().success).json(utils.getResponseJson(true,{},err? err.message : "No Record"))
            }
            res.status(utils.getReqStatus().success).json(utils.getResponseJson(false,pokemon,"Retrived!"))
        })
    }
    
    // Update a record by ID
    updateByID (req,res,next) {
        let pokemonID    = req.params.id;
        let reqPokemon   = utils.noInfections(req); 

        Pokemon.findById(pokemonID, (err,pokemon)=>{

            if(pokemon) {
                Object.assign(pokemon,reqPokemon);
                pokemon.save((err) => {
                    if(err){
                        res.status(utils.getReqStatus().error).json(utils.getResponseJson(true,{},err.message))
                    }
                    res.status(utils.getReqStatus().success).json(utils.getResponseJson(false,pokemon,"Updated!"))
                })
            } else{
                res.status(utils.getReqStatus().error).json(utils.getResponseJson(true,{},"No record"))
            }
            
        })
        
    }
    
    //Remove or Delete a record by ID
    removeByID (req,res,next) {
        Pokemon.deleteOne({
            _id: req.params.id
        }, function (err, result) {
            if (err) {
                res.status(utils.getReqStatus().error).json(utils.getResponseJson(true,{},err.message))
            }
            if(result.deletedCount > 1) {
                res.status(utils.getReqStatus().success).json(utils.getResponseJson(false,result,"Deleted!"))
            } else {
                res.status(utils.getReqStatus().success).json(utils.getResponseJson(false,result,"Not Deleted"))
            }
        })
    }


}

module.exports = new pokemonController();