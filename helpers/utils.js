/*
* Created custom function to get http status code
* Created custom response function to get default response json
* Created sanitizing function to sanitize input data avoiding xss attacks 
*/


const sanitizeHtml = require('sanitize-html');
const xss          = require('xss');

exports.getReqStatus = () => { 
    return {
        "error" : 500,
        "not_found" : 404,
        "success" : 200,
        "created": 201,
        "invalid" : 422
    }
}

exports.getResponseJson = (err,data,message) => {
    if (err){
        return {status: "Failed", data: null, message: message}
    } else {
        return {status: "Success", data: data, message: message}
    }
}

exports.noInfections = (req) => {
    let reqBody = req.body;

    Object.keys(reqBody).forEach((key)=>{ 
        let sanitizedItem = xss(reqBody[key]);
        reqBody[key] = sanitizedItem;
    });

    return reqBody;
}
